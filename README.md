# Advertiser
** Advertiser Repository**

This code repository was created for the iHeartMedia Advertiser coding challenge. The source code is available at *https://bitbucket.org/ivan2dx/advertiser/src*  This file contains a quick overview on how to run, test, and continue developing this codebase.

** Code Design and Structure**

The high-level structure of the code is as follows:

* advertiser
    * config - contains the swagger configuration class
	* controller
	* domain - contains the advertiser entity
	* exception - contains our custom exception classes, the custom error response class, and the Custom Exception Handler controller
	* repository - contains myBatis Mapper to DB schema
	* rest - contains a rest Request Object
	* service



** Error Handling and Logging Approach**

The exception package contains all the classes required to have a robust and centralized Exception handling methodology.
By using the ControllerAdvice we are able to route Exceptions trhough a single class, where we can add our custom Error classes as well as the custom response we want to send back.
Another advantage of this is that we can have a single location for logging Exceptions. Intead of logging (and sometimes forgetting to log!) everytime an excetion is thrown/caught in the controllers, services, or repos, we can log it just once, when we handle the exception at this high level. This increases the maintainability of our Error handling and Logging mechanisms.

** Known Issues **

1. The *'GET /api/advertiser/{name}'* and *'PUT /api/advertiser/{name}'* calls were being blocked on Swagger on my Chrome browser. I used IE to get around it. The direct API calls work just fine however (i.e. *http://localhost:8080/api/advertiser/Bob*).

** Future Improvements (Business budget permitting :) ) **

1. *Streamlined Validation.* Add Spring Validation (@Valid, @NotNul...) to entities and request objects. This will streamline validation as it will happen at binding time at the controller level, as opposed to manually having to validate the input on every Service layer function.

2. *More robust Exception handling.* Related to point 1. We can add more Error handling functions to our custom controller adviser, so that every Exception (including the Spring validation ones) are caught there.

3. *DTOs and Mappers.* As the code functionality and scope grows, we will need to hide the schema details by using DTOs instead of Entities as part of our rest responses. We can then add Mapstruct mappers to do the convertion for us.

** Test Coverage Adn Documentation**

The test classes can be found at *'advertiser/src/test/java'*. The test coverage report can be found at *'advertiser\documentation\Test Coverage Files'* Currently we have 100% coverage on Controller, Service and Domain layers.

A second report file contains sample screenshots of the Swagger main page, the actuators status (running on port 8090) and more coverage details. This is located at *'advertiser\documentation\Report'*.

The generated Jar is located at *advertiser\build\libs\advertiser-0.1-SNAPSHOT*. I have included a sample one in *'advertiser\documentation\'*.

** Running the code **

You can pull the code from the repository (*'git clone git@bitbucket.org:ivan2dx/advertiser.git'*) and run it using gradle on IntelliJ (as I did) or any other compatible IDE. You can then Run the AdvertiserApplication.

The application will be accessible at *'localhost:8080/swagger-ui.html#/'*, and the actuators at *'http://localhost:8090/actuator/[name]'*.
