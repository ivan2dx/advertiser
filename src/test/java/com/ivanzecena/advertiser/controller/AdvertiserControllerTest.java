package com.ivanzecena.advertiser.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivanzecena.advertiser.domain.Advertiser;
import com.ivanzecena.advertiser.rest.DeductCreditRequest;
import com.ivanzecena.advertiser.service.AdvertiserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class AdvertiserControllerTest {

    @InjectMocks
    private AdvertiserController advertiserController;

    @Mock
    private AdvertiserService advertiserService;
    private MockMvc mockMvc;
    private Advertiser advertiser;
    private String name;
    private Float amount;
    private List<Advertiser> advertiserList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(advertiserController).build();
        advertiser = new Advertiser();
        advertiser.setName("TestName");
        advertiserList = new ArrayList<>();
        advertiserList.add(advertiser);
        name = "TestName";
        amount = 15.5F;
    }

    @Test
    public void getAllAdvertisers() throws Exception{
        when(advertiserService.getAllAdvertisers()).thenReturn(advertiserList);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/advertiser"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAdvertiserByName() throws Exception{
        when(advertiserService.getAdvertiserByName(name)).thenReturn(advertiser);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/advertiser/TestName"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void createAdvertiser() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        when(advertiserService.createAdvertiser(advertiser)).thenReturn(advertiser);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/advertiser")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(advertiser)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void getAdvertiserIfValidCredit() throws Exception{
        when(advertiserService.getAdvertiserIfValidCredit(name, amount)).thenReturn(advertiser);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/advertiser/validCredit/TestName/14.5"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deductCreditFromAdvertiser() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        DeductCreditRequest deductCreditRequest = new DeductCreditRequest();
        deductCreditRequest.setDeductAmount(50.0F);
        when(advertiserService.deductCreditFromAdvertiser(name, deductCreditRequest)).thenReturn(advertiser);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/advertiser/deduct/TestName")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deductCreditRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAdvertiserByName() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        when(advertiserService.updateAdvertiserByName(name, advertiser)).thenReturn(advertiser);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/advertiser/TestName")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(advertiser)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAdvertiserByName() throws Exception{
        when(advertiserService.deleteAdvertiserByName(name)).thenReturn(true);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/advertiser/TestName"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
