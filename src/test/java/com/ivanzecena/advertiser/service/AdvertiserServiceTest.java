package com.ivanzecena.advertiser.service;

import com.ivanzecena.advertiser.domain.Advertiser;
import com.ivanzecena.advertiser.exception.BadRequestException;
import com.ivanzecena.advertiser.exception.ConflictException;
import com.ivanzecena.advertiser.exception.InternalServerException;
import com.ivanzecena.advertiser.exception.NotFoundException;
import com.ivanzecena.advertiser.repository.AdvertiserRepository;
import com.ivanzecena.advertiser.rest.DeductCreditRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class AdvertiserServiceTest {

    private AdvertiserService advertiserService;
    private Advertiser advertiser;
    private String name;
    private List<Advertiser> advertiserList;

    @Mock
    private AdvertiserRepository advertiserRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        advertiserService = new AdvertiserService(advertiserRepository);
        advertiser = new Advertiser();
        advertiser.setName("TestName");
        advertiser.setContactName("TestContactName");
        advertiser.setCreditLimit(1000.50F);
        advertiserList = new ArrayList<>();
        advertiserList.add(advertiser);
        advertiserList.add(advertiser);
        name = "TestName";
    }

    @Test
    public void getAllAdvertisers() {
        when(advertiserRepository.findAllAdvertisers()).thenReturn(advertiserList);

        List<Advertiser> response = advertiserService.getAllAdvertisers();
        assertNotNull(response);
        assertEquals(2, response.size());
    }

    @Test
    public void getAdvertiserByName() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);

        Advertiser response = advertiserService.getAdvertiserByName(name);
        assertNotNull(response);
        assertEquals(advertiser.getContactName(), response.getContactName());
    }

    @Test(expected = NotFoundException.class)
    public void getAdvertiserByNameNotFoundException() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(null);
        advertiserService.getAdvertiserByName(name);
    }

    @Test
    public void createAdvertiser() {
        when(advertiserRepository.findAdvertiserByName(advertiser.getName())).thenReturn(null);
        when(advertiserRepository.insertAdvertiser(advertiser)).thenReturn(1);

        Advertiser response = advertiserService.createAdvertiser(advertiser);
        assertNotNull(response);
        assertEquals(advertiser.getContactName(), response.getContactName());
    }

    @Test(expected = BadRequestException.class)
    public void createAdvertiserBadRequestException() {
        advertiser.setName(null);
        advertiserService.createAdvertiser(advertiser);
    }

    @Test(expected = ConflictException.class)
    public void createAdvertiserConflictException() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);
        advertiserService.createAdvertiser(advertiser);
    }

    @Test(expected = InternalServerException.class)
    public void createAdvertiserInternalServerException() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(null);
        when(advertiserRepository.insertAdvertiser(advertiser)).thenReturn(0);

        advertiserService.createAdvertiser(advertiser);
    }

    @Test
    public void updateAdvertiserByName() {
        when(advertiserRepository.findAdvertiserByName(advertiser.getName())).thenReturn(advertiser);
        when(advertiserRepository.updateAdvertiser(advertiser)).thenReturn(1);

        Advertiser response = advertiserService.updateAdvertiserByName(name, advertiser);
        assertNotNull(response);
        assertEquals(advertiser.getContactName(), response.getContactName());
    }

    @Test(expected = BadRequestException.class)
    public void updateAdvertiserBadRequestException() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);
        advertiserService.updateAdvertiserByName(name, null);
    }

    @Test(expected = NotFoundException.class)
    public void updateAdvertiserNotFoundException() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(null);
        advertiserService.updateAdvertiserByName(name, advertiser);
    }


    @Test(expected = InternalServerException.class)
    public void updateAdvertiserInternalServerException() {
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);
        when(advertiserRepository.updateAdvertiser(advertiser)).thenReturn(0);

        advertiserService.updateAdvertiserByName(name, advertiser);
    }

    @Test
    public void deleteAdvertiserByName() {
        when(advertiserRepository.findAdvertiserByName(advertiser.getName())).thenReturn(advertiser);
        when(advertiserRepository.deleteAdvertiserByName(name)).thenReturn(1);

        Boolean response = advertiserService.deleteAdvertiserByName(name);
        assertNotNull(response);
        assertEquals(true, response);
    }

    @Test(expected = NotFoundException.class)
    public void deleteAdvertiserByNameNotFoundException() {
        when(advertiserRepository.findAdvertiserByName(advertiser.getName())).thenReturn(null);
         advertiserService.deleteAdvertiserByName(name);
    }

    @Test(expected = InternalServerException.class)
    public void deleteAdvertiserByNameInternalServerException() {
        when(advertiserRepository.findAdvertiserByName(advertiser.getName())).thenReturn(advertiser);
        when(advertiserRepository.deleteAdvertiserByName(name)).thenReturn(0);

        advertiserService.deleteAdvertiserByName(name);
    }

    @Test
    public void deductCreditFromAdvertiser() {
        DeductCreditRequest deductCreditRequest = new DeductCreditRequest();
        deductCreditRequest.setDeductAmount(25.72F);
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);
        when(advertiserRepository.updateAdvertiser(advertiser)).thenReturn(1);

        Advertiser response = advertiserService.deductCreditFromAdvertiser(name, deductCreditRequest);
        assertNotNull(response);
        assertEquals(advertiser.getContactName(), response.getContactName());
    }

    @Test(expected = NotFoundException.class)
    public void deductCreditFromAdvertiserNotFoundException() {
        DeductCreditRequest deductCreditRequest = new DeductCreditRequest();
        deductCreditRequest.setDeductAmount(25.72F);
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(null);

        advertiserService.deductCreditFromAdvertiser(name, deductCreditRequest);
    }

    @Test(expected = InternalServerException.class)
    public void deductCreditFromAdvertiserInternalServerException() {
        DeductCreditRequest deductCreditRequest = new DeductCreditRequest();
        deductCreditRequest.setDeductAmount(25.72F);
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);
        when(advertiserRepository.updateAdvertiser(advertiser)).thenReturn(0);

        advertiserService.deductCreditFromAdvertiser(name, deductCreditRequest);
    }

    @Test(expected = BadRequestException.class)
    public void deductCreditFromAdvertiserBadRequestException() {
        advertiserService.deductCreditFromAdvertiser(name, null);
    }

    @Test
    public void getAdvertiserIfValidCredit() {
        Float amount = 25.75F;
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(advertiser);
        when(advertiserRepository.findAdvertiserByNameAndCreditLimit(name, amount)).thenReturn(advertiser);

        Advertiser response = advertiserService.getAdvertiserIfValidCredit(name, amount);
        assertNotNull(response);
        assertEquals(advertiser.getContactName(), response.getContactName());
    }

    @Test(expected = NotFoundException.class)
    public void getAdvertiserIfValidCreditNotFoundException() {
        Float amount = 25.75F;
        when(advertiserRepository.findAdvertiserByName(name)).thenReturn(null);

        advertiserService.getAdvertiserIfValidCredit(name, amount);
    }
}
