package com.ivanzecena.advertiser.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InternalServerException extends RuntimeException{
    private int appErrorCode;

    public InternalServerException(ErrorCodeEnum error) {
        super(error.getDescription());
        this.appErrorCode = error.getCode();
    }
}
