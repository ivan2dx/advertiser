package com.ivanzecena.advertiser.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConflictException extends RuntimeException{
    private int appErrorCode;

    public ConflictException(ErrorCodeEnum error) {
        super(error.getDescription());
        this.appErrorCode = error.getCode();
    }

}
