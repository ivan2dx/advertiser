package com.ivanzecena.advertiser.exception;

public enum ErrorCodeEnum {

    ADVERTISER_NAME_NOT_FOUND(1000, "This advertiser name was not found in DB."),
    ADVERTISER_ALREADY_EXISTS(1001, "This advertiser name already exists DB."),
    INVALID_ADVERTISER_PARAMETERS(1002, "Invalid Advertiser Request Parameters passed."),
    INVALID_DEDUCT_PARAMETERS(1003, "Invalid Deduct Request parameters passed."),
    UNABLE_TO_CREATE_ADVERTISER(1004, "Unable to create Advertiser in DB."),
    UNABLE_TO_UPDATE_ADVERTISER(1005, "Unable to update Advertiser in DB."),
    UNABLE_TO_DELETE_ADVERTISER(1006, "Unable to delete Advertiser in DB.");

    private ErrorCodeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    private int code;
    private String description;

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
