package com.ivanzecena.advertiser.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BadRequestException extends RuntimeException{
    private int appErrorCode;

    public BadRequestException(ErrorCodeEnum error) {
        super(error.getDescription());
        this.appErrorCode = error.getCode();
    }
}
