package com.ivanzecena.advertiser.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotFoundException extends RuntimeException {
    private int appErrorCode;

    public NotFoundException(ErrorCodeEnum error) {
        super(error.getDescription());
        this.appErrorCode = error.getCode();
    }
}
