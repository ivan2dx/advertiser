package com.ivanzecena.advertiser.exception;

import lombok.Data;

import java.util.Date;

@Data
public class ErrorResponse {
    private Date timestamp;
    private String message;
    private Integer appErrorCode;
    private String details;

    public ErrorResponse(Date timestamp, String message, Integer appErrorCode, String details) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.appErrorCode = appErrorCode;
        this.details = details;
    }

}
