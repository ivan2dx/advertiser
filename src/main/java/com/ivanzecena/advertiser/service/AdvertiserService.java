package com.ivanzecena.advertiser.service;

import com.ivanzecena.advertiser.domain.Advertiser;
import com.ivanzecena.advertiser.exception.*;
import com.ivanzecena.advertiser.repository.AdvertiserRepository;
import com.ivanzecena.advertiser.rest.DeductCreditRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdvertiserService {

    private AdvertiserRepository advertiserRepository;

    /**
     * Constructor with constructor dependency injection for the Advertiser Service
     *
     * @param advertiserRepository
     */
    @Autowired
    public AdvertiserService(AdvertiserRepository advertiserRepository) {
        this.advertiserRepository = advertiserRepository;
    }

    /**
     * This function finds all advertisers in the DB and returns them as a list
     *
     * @return a list of Advertisers
     */
    public List<Advertiser> getAllAdvertisers() {
        return advertiserRepository.findAllAdvertisers();
    }

    /**
     * This function finds an advertiser by advertiser name
     *
     * @param name
     * @return the advertiser object
     */
    public Advertiser getAdvertiserByName(String name) {
        Advertiser advertiser = advertiserRepository.findAdvertiserByName(name);
        if (advertiser == null) {
            throw new NotFoundException(ErrorCodeEnum.ADVERTISER_NAME_NOT_FOUND);
        }

        return advertiser;
    }

    /**
     * This function creates an Advertiser in the DB.
     *
     * @param advertiser
     * @return an Advertiser
     */
    public Advertiser createAdvertiser(Advertiser advertiser) {
        validateAdvertiser(advertiser);
        if (advertiserRepository.findAdvertiserByName(advertiser.getName()) != null) {
            throw new ConflictException(ErrorCodeEnum.ADVERTISER_ALREADY_EXISTS);
        }

        Integer status = advertiserRepository.insertAdvertiser(advertiser);
        if (status < 1) {
            throw new InternalServerException(ErrorCodeEnum.UNABLE_TO_CREATE_ADVERTISER);
        }

        return advertiser;
    }

    /**
     * This function will update an existing advertiser based on it name. It calls a helper function to copy the fields
     *
     * @param name
     * @param advertiser
     * @return the updated Advertiser
     */
    public Advertiser updateAdvertiserByName(String name, Advertiser advertiser) {
        Advertiser existingAdvertiser = advertiserRepository.findAdvertiserByName(name);
        if (existingAdvertiser == null) {
            throw new NotFoundException(ErrorCodeEnum.ADVERTISER_NAME_NOT_FOUND);
        }

        updateExistingAdvertiserFromRequest(existingAdvertiser, advertiser);
        Integer status = advertiserRepository.updateAdvertiser(existingAdvertiser);
        if (status < 1) {
            throw new InternalServerException(ErrorCodeEnum.UNABLE_TO_UPDATE_ADVERTISER);
        }

        return existingAdvertiser;
    }

    /**
     * This function deletes an advertiser by name.
     *
     * @param name
     * @return a true value if the deletion was successful, otherwise an error is thrown.
     */
    public Boolean deleteAdvertiserByName(String name) {
        if (advertiserRepository.findAdvertiserByName(name) == null) {
            throw new NotFoundException(ErrorCodeEnum.ADVERTISER_NAME_NOT_FOUND);
        }

        if (advertiserRepository.deleteAdvertiserByName(name) < 1) {
            throw new InternalServerException(ErrorCodeEnum.UNABLE_TO_DELETE_ADVERTISER);
        }

        return true;
    }

    /**
     * This function will deduct an amount from an existing advertisers credit value
     *
     * @param name
     * @param deductCreditRequest request object
     * @return the updated Advertiser
     */
    public Advertiser deductCreditFromAdvertiser(String name, DeductCreditRequest deductCreditRequest) {
        validateDeductCreditRequest(deductCreditRequest);
        Advertiser advertiser = advertiserRepository.findAdvertiserByName(name);
        if (advertiser == null) {
            throw new NotFoundException(ErrorCodeEnum.ADVERTISER_NAME_NOT_FOUND);
        }

        advertiser.setCreditLimit(advertiser.getCreditLimit() - deductCreditRequest.getDeductAmount());
        Integer status = advertiserRepository.updateAdvertiser(advertiser);
        if (status < 1) {
            throw new InternalServerException(ErrorCodeEnum.UNABLE_TO_UPDATE_ADVERTISER);
        }

        return advertiser;
    }

    /**
     * This function will return an advertise by name and if its credit is greater than or equal to the specified amount.
     *
     * @param name
     * @param amount
     * @return the matching advertiser
     */
    public Advertiser getAdvertiserIfValidCredit(String name, Float amount) {
        Advertiser advertiser = advertiserRepository.findAdvertiserByName(name);
        if (advertiser == null) {
            throw new NotFoundException(ErrorCodeEnum.ADVERTISER_NAME_NOT_FOUND);
        }

        return advertiserRepository.findAdvertiserByNameAndCreditLimit(name, amount);
    }


    //Helper functions

    /**
     * Helper function used to validate the fields of the Advertiser object
     *
     * @param advertiser
     */
    private void validateAdvertiser(Advertiser advertiser) {
        if (advertiser == null
                || advertiser.getName() == null
                || advertiser.getContactName() == null
                || advertiser.getCreditLimit() == null)
            throw new BadRequestException(ErrorCodeEnum.INVALID_ADVERTISER_PARAMETERS);
    }

    /**
     * Helper function used to validate the fields of the DeductCreditRequest object
     *
     * @param deductCreditRequest
     */
    private void validateDeductCreditRequest(DeductCreditRequest deductCreditRequest) {
        if (deductCreditRequest == null || deductCreditRequest.getDeductAmount() == null)
            throw new BadRequestException(ErrorCodeEnum.INVALID_DEDUCT_PARAMETERS);
    }

    /**
     * Helper function used to update an existing Advertiser based on the fields specified in a requestAdvertiser.
     *
     * @param existingAdvertiser
     * @param requestAdvertiser
     */
    private void updateExistingAdvertiserFromRequest(Advertiser existingAdvertiser, Advertiser requestAdvertiser) {
        if (requestAdvertiser == null) {
            throw new BadRequestException(ErrorCodeEnum.INVALID_ADVERTISER_PARAMETERS);
        }
        if (requestAdvertiser.getContactName() != null) {
            existingAdvertiser.setContactName(requestAdvertiser.getContactName());
        }
        if (requestAdvertiser.getCreditLimit() != null) {
            existingAdvertiser.setCreditLimit(requestAdvertiser.getCreditLimit());
        }
    }
}
