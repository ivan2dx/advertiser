package com.ivanzecena.advertiser.rest;

import lombok.Data;

@Data
public class DeductCreditRequest {
    Float deductAmount;
}
