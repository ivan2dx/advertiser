package com.ivanzecena.advertiser.controller;

import com.ivanzecena.advertiser.domain.Advertiser;
import com.ivanzecena.advertiser.rest.DeductCreditRequest;
import com.ivanzecena.advertiser.service.AdvertiserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created By Ivan Zecena.
 */

@RestController
@Api(tags = "Advertiser", description = "Manipulate Advertisers in the system")
@RequestMapping(value="/api/advertiser")
public class AdvertiserController {

    private AdvertiserService advertiserService;

    public AdvertiserController(AdvertiserService advertiserService) {
        this.advertiserService = advertiserService;
    }

    @ApiOperation(value = "Get a list of all advertiser")
    @GetMapping()
    public ResponseEntity<List<Advertiser>> getAllAdvertisers() {
        return new ResponseEntity<>(advertiserService.getAllAdvertisers(), HttpStatus.OK);
    }

    @ApiOperation(value = "Get an advertiser by name")
    @GetMapping("/{name}")
    public ResponseEntity<Advertiser> getAdvertiserByName(@PathVariable String name) {
        return new ResponseEntity<>(advertiserService.getAdvertiserByName(name), HttpStatus.OK);
    }

    @ApiOperation(value = "Create an advertiser")
    @PostMapping()
    public ResponseEntity<Advertiser> createAdvertiser(@RequestBody Advertiser advertiser) {
        return new ResponseEntity<>(advertiserService.createAdvertiser(advertiser), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Get an advertiser if its credit is greater than or equal to specified amount")
    @GetMapping("/validCredit/{name}/{amount}")
    public ResponseEntity<Advertiser> getAdvertiserIfValidCredit(@PathVariable String name, @PathVariable Float amount) {
        return new ResponseEntity<>(advertiserService.getAdvertiserIfValidCredit(name, amount), HttpStatus.OK);
    }


    @ApiOperation(value = "Deduct credit from an advertiser's account")
    @PostMapping("/deduct/{name}")
    public ResponseEntity<Advertiser> deductCreditFromAdvertiser(@PathVariable String name, @RequestBody DeductCreditRequest deductCreditRequest) {
        return new ResponseEntity<>(advertiserService.deductCreditFromAdvertiser(name, deductCreditRequest), HttpStatus.OK);
    }

    @ApiOperation(value = "Update an advertiser")
    @PutMapping("/{name}")
    public ResponseEntity<Advertiser> updateAdvertiserByName(@PathVariable String name, @RequestBody Advertiser advertiser) {
        return new ResponseEntity<>(advertiserService.updateAdvertiserByName(name, advertiser), HttpStatus.OK);
    }

    @ApiOperation(value = "Delete an advertiser")
    @DeleteMapping("{name}")
    public ResponseEntity<Boolean> deleteAdvertiserByName(@PathVariable String name) {
        return new ResponseEntity<>(advertiserService.deleteAdvertiserByName(name), HttpStatus.OK);
    }

}
