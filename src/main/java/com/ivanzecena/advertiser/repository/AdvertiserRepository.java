package com.ivanzecena.advertiser.repository;

import com.ivanzecena.advertiser.domain.Advertiser;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AdvertiserRepository {

    @Select("SELECT * FROM advertiser")
    List<Advertiser> findAllAdvertisers();

    @Select("SELECT * FROM advertiser WHERE name = #{name}")
    Advertiser findAdvertiserByName(String name);

    @Insert("INSERT INTO advertiser(name, contact_name, credit_limit) " +
            "VALUES (#{name}, #{contactName}, #{creditLimit})")
    Integer insertAdvertiser(Advertiser advertiser);

    @Update("UPDATE advertiser set name = #{name}, contact_name = #{contactName}," +
            " credit_limit = #{creditLimit} where name = #{name}")
    Integer updateAdvertiser(Advertiser advertiser);

    @Delete("DELETE FROM advertiser WHERE name = #{name}")
    Integer deleteAdvertiserByName(String name);

    @Select("SELECT * FROM advertiser WHERE name = #{name} AND credit_limit >= #{creditLimit}")
    Advertiser findAdvertiserByNameAndCreditLimit(@Param("name") String name, @Param("creditLimit") Float amount);
}