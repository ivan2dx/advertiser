package com.ivanzecena.advertiser.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class Advertiser implements Serializable {
    private String name;
    private String contactName;
    private Float creditLimit;
}
