--Script that creates the schema and populates the table on each startup

--dropping existing table when starting
drop table if exists advertiser;

--creating table in DB
create table advertiser (name varchar, contact_name varchar, credit_limit float);

--Populating DB
insert into advertiser (name, contact_name, credit_limit) values ('Mike', 'Mikeson', 500.0);
insert into advertiser (name, contact_name, credit_limit) values ('Bob', 'Bobson', 100.5);
insert into advertiser (name, contact_name, credit_limit) values ('Tyler', 'Tylerson', 20.0);
insert into advertiser (name, contact_name, credit_limit) values ('Johny', 'Johnson', 30.0);
